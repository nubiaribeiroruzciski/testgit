package caraoucoroa;

import java.util.Scanner;

/**
 *
 * @author ALUNO
 */
public class CaraOuCoroa {

    final static Scanner LER = new Scanner(System.in);

    public static void main(String[] args) {

        int N2 = 0;

        N = lerN(N);

        verificarVencedores(N);

    }

    public static int lerN(int N) {

        N = LER.nextInt();
        return N;

    }

    public static int lerVezesVencidas(int vv) {

        vv = LER.nextInt();
        return vv;

    }

    public static void verificarVencedores(int N) {

        N = lerN(N);
        int i = 0;
        int x = 0;
        int y = 0;
        int num = 0;

        do {

            num = lerVezesVencidas(num);
            if (num == 0) {

                x++;
            } else if (num == 1) {

                y++;
            }

            i++;
        } while (i < N);

        imprimirVencedores(x, y);

    }

    public static void imprimirVencedores(int x, int y) {

        System.out.println("Mary won " + x + " times and John won " + y + " times");

    }

}
